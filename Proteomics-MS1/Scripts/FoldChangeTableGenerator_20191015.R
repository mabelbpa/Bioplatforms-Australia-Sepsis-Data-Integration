library(tidyverse)


fcfiles=list.files("F:/BPA_Outputs/20191015",pattern = "Result\\.csv$",full.names = TRUE,recursive = TRUE)
ssemap<-read_csv("F:/BPA_Outputs/SpStrainCodes.csv")
colfilt <- c("Protein IDs", "Sera_vs_RPMI_log2 fold change", "Sera_vs_RPMI.galactose_log2 fold change",	"Sera_vs_RPMI.glucose_log2 fold change", "Sera_vs_RPMI_p.adj", "Sera_vs_RPMI.galactose_p.adj",	"Sera_vs_RPMI.glucose_p.adj")
rename_map <- read_csv("F:/BPA_Outputs/HeaderRenameMap.csv")

generate_FCTable<-function(x){
  strainID <- as.list(str_split(x,"/",simplify = TRUE))
  strainID <- strainID[length(strainID)-1][[1]]
  strainID<- str_remove(strainID,"^.+_")
  print(strainID)
  fct<-read_csv(x)
#  row.names(fct)
  fctg<-fct%>%select_if(colnames(fct) %in% (colfilt))%>%
    gather(key, value, -"Protein IDs")%>%
    extract(key,c("Comparisons","property"),"(.+)_([^_]+)$")%>%
    spread(property,value)%>%
    add_column("StrainID"=strainID)%>%
    rename(logFC=`log2 fold change`,FDR=p.adj,`Entity ID / Protein Accession / Gene Accession`=`Protein IDs`)
#  print(fctg)
  return(fctg)
}

fcts<-lapply(fcfiles, generate_FCTable)

fct<-read_tsv("F:/P16_0062_BPA_Sepsis-selection/Igy MPMF Fold change table v1.tsv")
sapply(fcts, function(x){.GlobalEnv$fct<- rbind(fct,x)})
fctj<-left_join(fct,ssemap,by=c(StrainID="Code"))%>%
  rename(`Strain Name`=Strain)%>%
  select("Organism","Strain Name","Type of Experiment","Entity ID / Protein Accession / Gene Accession","Comparisons","logFC","FDR")%>%
  left_join(rename_map)%>%
  mutate(Comparisons=NULL)%>%
  rename(Comparisons = ComparisonsNew)%>%
  select("Organism","Strain Name","Type of Experiment","Entity ID / Protein Accession / Gene Accession","Comparisons","logFC","FDR")
write_csv(fctj,"F:/BPA_Outputs/20191015/MPMF FoldChange table v20191015.csv")


fctj_summary<- fctj%>%group_by(`Entity ID / Protein Accession / Gene Accession`,`Comparisons`,`Organism`)%>%summarise(n = n())
fctj_summary<- fctj%>%group_by(`Entity ID / Protein Accession / Gene Accession`)%>%summarise(n = n())%>%arrange(desc(n))%>%head()


#fctsummary<-fctj%>%group_by(`Entity ID / Protein Accession / Gene Accession`)%>%summarise(Strain_count = count(`Strain Name`),Organism_Count = count(`Organism`))
#  spread(key = sample,value = `LogCounts (one for sera, one for RPMI)`)%>%group_by(`Entity ID / Protein Accession / Gene Accession`)%>%summarise(mean = mean)
