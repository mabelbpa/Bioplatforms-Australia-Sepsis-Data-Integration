---
title: "Parse proteomics-MS1 master fold-change table"
output: html_notebook
---

```{r}
if( !require(pacman)) {
  install.packages("pacman")
  library(pacman)
}

p_load(tidyverse)
p_load(here)
p_load(readxl)
p_load(tidyselect)

base_dir <- here::here() # "/home/ignatius/PostDoc/2019/Sepsis"
source( file.path( base_dir, "Source/Common/helper_functions.R") )
source( file.path( base_dir, "Source/Common/common_parameters.R") )


```

## Global Parameters
```{r}
my_e_value_cutoff <- 10^-3
version_date <- master_table_version_date

```

## Directories Management
```{r}

proteomics_MS1_data_dir <- file.path( owncloud_dir, "Proteomics-MS1")
# gff_data_dir <- file.path( data_dir, "RAW_OMICS_DATA/Annotations and Mapping/GFF" ) 
# genome_statistics_file <- file.path( data_dir, "Genomes_List/num_seq_per_strain_and_accession.xlsx") 
proteomics_MS1_master_table_dir <- file.path( owncloud_dir, "Master_Tables", version_date, "Proteomics-MS1" ) 
proteomics_MS1_fold_changes_dir <- file.path(proteomics_MS1_master_table_dir, "Fold_Changes" )

create_dir_if_not_exists(proteomics_MS1_master_table_dir)
create_dir_if_not_exists(proteomics_MS1_fold_changes_dir)
```

## Files Management
```{r}
hits_to_pathway_ko_table_file <- file.path( owncloud_dir, 
                                            paste("Annotations_and_Mapping/KEGG_Annotations_Stable_Versions/Version_2/E_value_", 
                                                  my_e_value_cutoff, 
                                                  sep=""),
                                            "Map_to_Uniprot/mapping_uniprot_hits_to_pathways_and_ko.tsv" ) 

log_fold_change_file <- file.path( proteomics_MS1_data_dir, "MPMF FoldChange table v20191015.csv" )


genes_list <-  vroom::vroom( file.path( owncloud_dir, "Master_Tables/20190807/Transcriptomics/Fold_Changes/genes_list.tsv" )  )


proteomics_MS1_logFC_FDR_table_file <- file.path(proteomics_MS1_fold_changes_dir, 
                                                        "proteomics-MS1_master_logFC_FDR_table.tsv")

proteomics_MS1_kegg_logFC_FDR_table_file <- file.path(proteomics_MS1_fold_changes_dir, 
                                                        "proteomics-MS1_kegg_logFC_FDR_table.tsv")

```



## Read refseq protein ID to Uniprot to KEGG pathways data
```{r}
hits_to_pathway_ko_table  <- read_tsv( hits_to_pathway_ko_table_file  )
hits_to_pathway_ko_table_cleaned <- hits_to_pathway_ko_table %>%
                      filter(  evalue < my_e_value_cutoff) %>%
                      dplyr::select( Query_Species, Query_Strain, query_acc.ver, kegg_pathway_id, kegg_ko_id) %>%
                      dplyr::rename( Species = "Query_Species",
                              Strain = "Query_Strain",
                              entity_id = "query_acc.ver") %>%
                      as.tibble()

```

## Add the locus tags into the tables
```{r}
genes_list <-  vroom::vroom( file.path( owncloud_dir, "Master_Tables/20190807/Transcriptomics/Fold_Changes/genes_list.tsv" )  )


refseq_id_to_locus_tag <- genes_list %>%
                          dplyr::filter( is.na(pseudo) &
                                           !is.na(protein_id)) %>%
                          dplyr::distinct( Species, Strain, protein_id, locus_tag ) %>%
                          dplyr::group_by( Species, Strain, protein_id ) %>%
                          summarise( additional_id = paste(locus_tag, collapse=";")    ) %>%
                          ungroup()

refseq_id_to_locus_tag %>%
  filter( str_detect( additional_id, "\\;")) %>%
  head()


```


## Read proteomics-MS1 logFC and FDR values
```{r}
proteomics_MS1_log_fc <-  read_csv( log_fold_change_file ) 

colnames(proteomics_MS1_log_fc)

proteomics_MS1_log_fc_cleaned <- proteomics_MS1_log_fc %>%
                                      dplyr::select(-matches( "X\\d")) %>%
                                    left_join( refseq_id_to_locus_tag, by=c( "Organism" = "Species", 
                                                                             "Strain Name" = "Strain", 
                                                                             "Entity ID / Protein Accession / Gene Accession" = "protein_id"))

length( colnames(proteomics_MS1_log_fc_cleaned) )
colnames( proteomics_MS1_log_fc_cleaned) <- c("Species", "Strain", "Type_of_Experiment", "entity_id", "Comparisons", "logFC", "FDR",
                                              "additional_id")

#Organism,	Strain Name,	Type of Experiment,	Entity ID / Protein Accession / Gene Accession,	Comparisons,	logFC,	FDR

write_tsv( proteomics_MS1_log_fc_cleaned, 
           path=proteomics_MS1_logFC_FDR_table_file ) 

```

## Merge with KEGG pathway ID 
```{r}
proteomics_MS1_kegg_logFC_FDR_table <- proteomics_MS1_log_fc_cleaned %>%
  left_join( hits_to_pathway_ko_table_cleaned, by = c("Species", "Strain", "entity_id") )

write_tsv( proteomics_MS1_kegg_logFC_FDR_table, 
           path=proteomics_MS1_kegg_logFC_FDR_table_file ) 

```

