---
title: "R Notebook"
output: html_notebook
---


```{r}
if( !require(pacman)) {
  install.packages("pacman")
  library(pacman)
}

p_load(tidyverse)
p_load(here)
p_load(readxl)
p_load(tidyselect)
p_load(rtracklayer )
p_load(vroom)

base_dir <- here::here() # "/home/ignatius/PostDoc/2019/Sepsis"
source( file.path( base_dir, "Source/Common/helper_functions.R") )
source( file.path( base_dir, "Source/Common/common_parameters.R") )


```

## Global Parameters
```{r}
my_e_value_cutoff <- 10^-3
version_date <- master_table_version_date

```

## Directories Management
```{r}

transcriptomics_data_dir <- file.path( owncloud_dir, "Transcriptomics")
gff_data_dir <- file.path( owncloud_dir, "Annotations_and_Mapping/GFF" ) 
genome_statistics_file <- file.path( data_dir, "Genomes_List/num_seq_per_strain_and_accession.xlsx") 
transcriptomics_master_table_dir <- file.path( owncloud_dir, "Master_Tables", version_date, "Transcriptomics" ) 
transcriptomics_fold_change_dir <- file.path( transcriptomics_master_table_dir, "Fold_Changes" ) 

create_dir_if_not_exists(transcriptomics_master_table_dir)
create_dir_if_not_exists(transcriptomics_fold_change_dir)

```

## Files management
```{r}

hits_to_pathway_ko_table_file <- file.path( owncloud_dir, 
                                            paste("Annotations_and_Mapping/KEGG_Annotations_Stable_Versions/Version_2/E_value_", 
                                                  my_e_value_cutoff, 
                                                  sep=""),
                                            "Map_to_Uniprot/mapping_uniprot_hits_to_pathways_and_ko.tsv" ) 



list_of_de_genes_files_pattern <- file.path( transcriptomics_data_dir, "bpa_sepsis_summary/*/*/gene_expression_analysis/*_vs_*.txt" )

gff_master_table_file <- file.path( transcriptomics_fold_change_dir, "genes_list.tsv" )

transcriptomics_logFC_fdr_values_file <- file.path(transcriptomics_fold_change_dir, "transcriptomics_logFC_fdr_values.tsv") 

transcriptomics_master_logFC_fdr_values_file <- file.path(transcriptomics_fold_change_dir, "transcriptomics_master_logFC_fdr_values.tsv")

transcriptomics_kegg_logFC_fdr_values_file <- file.path(transcriptomics_fold_change_dir, "transcriptomics_kegg_logFC_fdr_values.tsv")

transcriptomics_non_coding_and_psedo_genes_table_file <- file.path(transcriptomics_fold_change_dir, "transcriptomics_non_protein_coding_and_psedo_genes_master_logFC_fdr_values.tsv")

```


## Read Genome statistics
```{r}
strain_to_accessions   <- read_xlsx(path=genome_statistics_file, sheet="strain_to_accessions")
strain_to_accessions_cleaned <- strain_to_accessions %>%
                                dplyr::select( Species, Strain, refseq_ids) %>%
                                separate_rows( refseq_ids, sep = "; ") %>%
                                dplyr::rename( chromosome_refseq_id = "refseq_ids") %>%
                                mutate( Species_Short = str_replace( Species, 
                                                                     "([A-Z])\\w+\\s([a-z]+)", 
                                                                     "\\1\\2"))

```

## Read refseq protein ID to Uniprot to KEGG pathways data
```{r}

hits_to_pathway_ko_table  <- vroom::vroom( hits_to_pathway_ko_table_file  )

hits_to_pathway_ko_table_cleaned <- hits_to_pathway_ko_table %>%
  dplyr::filter(  evalue < my_e_value_cutoff) %>%
  dplyr::select( Query_Species, Query_Strain, 
                 query_acc.ver, kegg_pathway_id, kegg_ko_id) %>%
  dplyr::rename( Species = "Query_Species",
                 Strain = "Query_Strain",
                 entity_id = "query_acc.ver",
                 cleaned_pathway_id = "kegg_pathway_id",
                 ko_id = "kegg_ko_id") %>%
  distinct() %>%
  as.tibble()

```


## Find the list of differentially expressed genes files to parse 
```{r}

list_of_de_genes_files <- Sys.glob( list_of_de_genes_files_pattern   ) 

```

## Find list of GFF files 
```{r}

list_of_gff_files <- Sys.glob( file.path(gff_data_dir, "*.gff") )
```



## Build differentially expressed genes files table 
```{r}

position_for_dummy_column_marker <- which( strsplit( list_of_de_genes_files[1], "/")[[1]]  == "bpa_sepsis_summary")

list_of_columns <- c( paste( "X", 1:(position_for_dummy_column_marker+1), sep=""),
                        "Strain_Dir",  
                      paste( "X", (position_for_dummy_column_marker+3), sep="") , 
                      "de_file" )

# Analyses are RPMI vs Pooled_sera
# Except S pneumoniae which was carried out as Pooled_sera-RPMI_glucose,Pooled_sera-RPMI_galactose,RPMI_glucose-RPMI_galactose

files_table <-  data.frame(  files = list_of_de_genes_files )  %>%
                mutate( temp_files = files ) %>%
                separate( temp_files, sep="/", into=list_of_columns)  %>%
                dplyr::select( - matches("X\\d") ) %>%
                mutate( Strain_Dir =  str_replace( Strain_Dir,  "180_15",   "180/15"))  %>%
                mutate( Strain_Dir =   str_replace( Strain_Dir,  "180_2",   "180/2"))  %>%
                mutate( Comparisons = case_when ( str_detect( de_file, "[Ss]era.*?[Gg]alactose" ) ~ "Sera vs. RPMI + Galactose",
                                                 str_detect( de_file, "[Ss]era.*?[Gg]lucose" ) ~ "Sera vs. RPMI + Glucose",
                                                 str_detect( de_file, "[Gg]alactose.*?[Gg]lucose" ) ~ "RPMI + Galactose vs. RPMI + Glucose",
                                                 str_detect( de_file, "[Gg]lucose.*?[Gg]alactose" ) ~ "RPMI + Galactose vs. RPMI + Glucose",
                                                 str_detect( de_file, "[Ss]era.*?(RPMI|rpmi)") ~ "Sera vs. RPMI",
                                                 TRUE ~ NA_character_  )) %>%
                separate( Strain_Dir, "_", into=c("Species_Short", "Strain")) %>%
                mutate( files = as.character(files)) 

files_table %>% dplyr::select( Species_Short, Strain )
files_table %>% dplyr::distinct( Species_Short )

files_table_cleaned <- files_table %>%
                        left_join( strain_to_accessions_cleaned, by=c("Species_Short" = "Species_Short",
                                                                       "Strain" = "Strain") )  %>%
                        dplyr::select(- chromosome_refseq_id) %>%
                        distinct() 

```


## Function to parse one GFF file 
```{r} 
parse_one_gff_file <- function( gff_file ) {
  
   temp_gff <- readGFF(gff_file)

 gene_table <- temp_gff %>%
   as.data.frame %>%
   filter( type=="gene" | type=="pseudogene") %>%
   dplyr::select( seqid, start, end, strand, Name, locus_tag, ID) %>%
   dplyr::rename( gene_name = "Name")
 
 cds_table <- temp_gff %>%
   as.data.frame %>%
   filter( type=="CDS") %>%
   dplyr::select( seqid,  start, end, strand, locus_tag, protein_id, Name, product, pseudo) %>%
   dplyr::rename( protein_name = "Name")
 
 
 gene_cds_table_part_a <- gene_table %>%
   left_join(cds_table, by = c("seqid", "locus_tag") ) 
 
 
 gene_cds_table_part_b <- gene_table %>%
   left_join(cds_table, by = c("seqid", "start", "end", "strand") )
 
 gene_cds_table <- gene_cds_table_part_a %>%
   bind_rows(gene_cds_table_part_b)
 
 
 return( gene_cds_table ) 
  
}

readGFF(list_of_gff_files[17]) %>%
   as.data.frame %>%
   filter( type=="gene" | type=="pseudogene") %>%
  filter( locus_tag == "EW036_RS10995") 

```


## Parse GFF tables
```{r}

list_of_gff_tables <- purrr::map( list_of_gff_files, parse_one_gff_file )

gff_table <- list_of_gff_tables %>% 
              bind_rows() %>%
              left_join( strain_to_accessions_cleaned, by=c("seqid" = "chromosome_refseq_id")) %>%
              mutate( transcript_id  = ifelse( is.na(pseudo),  gene_name, ID ))  %>%
              mutate( locus_tag = coalesce(locus_tag.x, locus_tag.y))  %>%
              dplyr::mutate( ID = str_replace_all( ID, "gene-", "")) %>%
              dplyr::mutate( locus_tag = coalesce(locus_tag, ID)) %>%
              dplyr::select( -locus_tag.x, -locus_tag.y) 


write_tsv( gff_table, path=gff_master_table_file )

gff_table_edited <- gff_table %>%
  dplyr::select( -start.x, -end.x, -strand.x,  -start.y, -end.y, -strand.y, -start, -end, -strand)

## Check one pseudo gene 
gff_table %>%
  filter( locus_tag == "EW036_RS10995")

## Shows genes with multiple copies (more than one copy) per chromosome
gff_table %>%
  inner_join( gff_table %>% 
                group_by( seqid, transcript_id) %>%
                summarise( counts=n()) %>%
                filter ( counts > 1) , 
               by = c("seqid", "transcript_id")) %>%
  arrange( seqid, transcript_id)


 list_of_gff_tables [[ which(purrr::map_lgl( list_of_gff_files, ~str_detect(., "GCF_900619605.1") ) ) ]] %>%
   filter(protein_name == "WP_104021446.1")
```


```{r}


refseq_id_to_locus_tag <- gff_table %>%
                          dplyr::distinct( Species, Strain, protein_id, locus_tag, ID )  %>%
                          dplyr::mutate( ID = str_replace_all( ID, "gene-", "")) %>%
                          dplyr::mutate( locus_tag = coalesce(ID, locus_tag)) %>%
                          dplyr::select(-ID) %>%
                          dplyr::filter( !is.na(protein_id) & !is.na(locus_tag)) %>%
                          dplyr::group_by( Species, Strain, protein_id ) %>%
                          summarise( additional_id = paste(locus_tag, collapse=";")    ) %>%
                          ungroup()


locus_tag_to_refseq_id  <- gff_table %>%
                          dplyr::filter( is.na(pseudo) &
                                           !is.na(protein_id)) %>% 
                          dplyr::distinct( Species, Strain, protein_id, locus_tag, ID )  %>%
                          dplyr::mutate( ID = str_replace_all( ID, "gene-", "")) %>%
                          dplyr::mutate( locus_tag = coalesce(ID, locus_tag)) %>%
                          dplyr::select(-ID) %>%
                          dplyr::filter( !is.na(protein_id) & !is.na(locus_tag)) %>%
                          dplyr::distinct( Species, Strain, protein_id, locus_tag ) 
```



```{r} 
   temp_gff <- readGFF( list_of_gff_files[[ which(purrr::map_lgl( list_of_gff_files, ~str_detect(., "GCF_900619605.1") ) ) ]])

temp_gff %>% 
  as.data.frame() %>%
  dplyr::filter( start == 1086402	 & end == 1086732)

```



## Function to Parse de genes file 
```{r}

# Function to clean the table 
read_de_genes_table <- function(x) {  
  
  
  output_table <- vroom::vroom(x, delim="\t", skip = 1, col_names=c("gene_ids", "logFC",	"AveExpr",	"t",	"P.Value",	"adj.P.Val",	"B")) %>% 
                      as.data.frame 
  
  
  if (  any("adj.P.Val" %in% colnames( output_table   )) ) {
    
    output_table <- output_table  %>%
                    dplyr::rename( FDR = "adj.P.Val" ) 
  } 
  
    if (  any("AveExpr" %in% colnames( output_table   )) ) {
    
    output_table <- output_table  %>%
                    dplyr::rename( logCPM = "AveExpr" ) 
    } 
  
      if (  any("PValue" %in% colnames( output_table   )) ) {
    
    output_table <- output_table  %>%
                    dplyr::rename( P.Value = "PValue" ) 
  } 

  
  return( output_table)
}
```

## Parse DE genes results
```{r}
de_genes_table_nested <- files_table_cleaned  %>%
  mutate( temp_de_genes =  purrr::map( files, read_de_genes_table) ) %>%
  dplyr::select(-files, -de_file)  


de_genes_table_nested


de_genes_table <- de_genes_table_nested %>%
  tidyr::unnest( cols=temp_de_genes )  %>%
  mutate( gene_ids = case_when( gene_ids == "aph(3)-Ib" ~ "aph(3'')-Ib", 
                                gene_ids == "aac_6prime_Ib" ~ "aac(6')-Ib", 
                                gene_ids == "aph_3prime_IIIa"  ~ "aph(3')-IIIa",
                                TRUE ~ gene_ids) )

de_genes_table_with_ids_part_A <- de_genes_table %>%
  inner_join( gff_table_edited, by =c ( "gene_ids" = "gene_name", 
                                 "Species" = "Species",
                                 "Strain" = "Strain", 
                                 "Species_Short" = "Species_Short") ) %>%
  dplyr::select( -locus_tag) %>%
  dplyr::rename( chromosome_refseq_id = "seqid") %>%
  distinct()

de_genes_table_with_ids_part_B <- de_genes_table %>%
  inner_join( gff_table_edited, by =c ( "gene_ids" = "transcript_id", 
                                 "Species" = "Species",
                                 "Strain" = "Strain", 
                                 "Species_Short" = "Species_Short") ) %>%
  dplyr::select(  -locus_tag) %>%
  dplyr::rename( chromosome_refseq_id = "seqid") %>%
  distinct()


de_genes_table_with_ids_part_C <- de_genes_table %>%
  inner_join( gff_table_edited, by =c ( "gene_ids" = "ID", 
                                 "Species" = "Species",
                                 "Strain" = "Strain", 
                                 "Species_Short" = "Species_Short") ) %>%
  dplyr::select(  -locus_tag) %>%
  dplyr::rename( chromosome_refseq_id = "seqid") %>%
  distinct()


de_genes_table_with_ids_part_D <- de_genes_table %>%
  inner_join( gff_table_edited, by =c ( "gene_ids" = "locus_tag", 
                                 "Species" = "Species",
                                 "Strain" = "Strain", 
                                 "Species_Short" = "Species_Short") ) %>%
  dplyr::rename( chromosome_refseq_id = "seqid") %>%
  distinct()


de_genes_table_with_ids <- de_genes_table_with_ids_part_A  %>%
  bind_rows( de_genes_table_with_ids_part_B) %>%
  bind_rows( de_genes_table_with_ids_part_C) %>%
  bind_rows( de_genes_table_with_ids_part_D) %>%
  distinct() %>%
  as.tibble() %>%
  dplyr::mutate( logFC = case_when ( str_detect( Comparisons,  "Sera vs. RPMI") ~ -logFC ,				
   str_detect( Comparisons,   "Sera vs. RPMI + Galactose") ~ logFC,				
   str_detect( Comparisons,   "Sera vs. RPMI + Glucose") ~  logFC,				
   str_detect( Comparisons,   "RPMI + Galactose vs. RPMI + Glucose") ~  -logFC ,
   TRUE ~ logFC  ) )  %>%
  left_join( locus_tag_to_refseq_id %>% 
               filter(!is.na(protein_id)), 
             by=c("Species" = "Species", 
                  "Strain" = "Strain", 
                  "protein_id" ="protein_id") )  %>%
  distinct()

# de_genes_table_with_ids  %>% 
#   filter( Strain == "BPH2760" & protein_id == "WP_000616840.1")  

```

## Checking Missing Entries
```{R}

missing_entries <- de_genes_table %>%
  distinct( gene_ids, Species, Strain, Species_Short) %>%
  dplyr::setdiff( de_genes_table_with_ids %>% 
  distinct( gene_ids, Species, Strain, Species_Short) )  

if( nrow(missing_entries) > 0 ) {
  
  stop("There are entries without protein_refseq_id mapped.")
}


```
 
 

###  Check wether the number of rows matched are exactly the same 
```{r}
nrow(de_genes_table )

de_genes_table_with_ids %>%
  dplyr::select( -protein_name, -protein_id, -ID, -transcript_id, -pseudo, -gene_name, -product, -chromosome_refseq_id, -locus_tag ) %>%
  distinct() %>%
  nrow()

if( nrow(de_genes_table ) != de_genes_table_with_ids %>%
  dplyr::select( -protein_name, -protein_id, -ID, -transcript_id, -pseudo, -gene_name, -product, -chromosome_refseq_id, -locus_tag ) %>%
  distinct() %>%
  nrow() ) {
  
  stop("Problem with ID merging")
}



```

## Write all fold-change values merged with gene IDs
```{r}


# de_genes_table_with_ids
write_tsv( de_genes_table_with_ids, 
           path=transcriptomics_logFC_fdr_values_file) 

```


## Create master table for protein coding genes only
```{r}

colnames(de_genes_table_with_ids)

locus_tag_to_refseq_id %>%
  filter( Species != "Streptococcus pneumoniae") %>%
  filter( is.na(locus_tag))

de_genes_table_with_ids_master <- de_genes_table_with_ids %>%
  filter( is.na(pseudo) & !is.na( protein_id)) %>%
  mutate( Type_of_Experiment = "RNA-Seq") %>%
  mutate(   additional_id=  protein_id) %>%

  mutate( entity_id = coalesce( str_replace_all ( ID, "^gene-", ""),
                                str_replace_all ( locus_tag, "^gene-", "") ) ) %>%
  dplyr::distinct( Species, Strain, Type_of_Experiment, entity_id, Comparisons, logFC, FDR, additional_id )

write_tsv( de_genes_table_with_ids_master, 
           path=transcriptomics_master_logFC_fdr_values_file ) 

```


```{r}
de_genes_table_with_ids_master %>%
  filter( is.na(entity_id))

de_genes_table_with_ids %>%
  filter( is.na(pseudo)) %>%
  mutate( Type_of_Experiment = "RNA-Seq") %>%
  mutate( entity_id =  case_when ( !is.na(protein_id) ~ protein_id, 
                                   # tRNAs are not pseudo genes and are not proteins, use entity_id for these
                                  is.na(protein_id) & is.na(pseudo) ~ str_replace( gene_ids, "^gene-", "" ), 
                                  TRUE ~ NA_character_ ) ) %>%
  filter( is.na(entity_id))


de_genes_table_with_ids %>%
  filter( is.na(pseudo ) & is.na(protein_id))


gff_table %>%
  filter( is.na(pseudo ) & is.na(protein_id))


de_genes_table_with_ids %>%
  filter( !is.na(pseudo) ) %>%
  filter ( !str_detect( gene_ids, "EW"))


de_genes_table_with_ids %>%
  filter(  is.na(protein_id)  & is.na(pseudo) ) %>%
  filter ( !str_detect( gene_ids, "EW"))

```


## Merge with KEGG pathway ID 
```{r}

transcriptomics_kegg_logFC_FDR_table <- de_genes_table_with_ids_master %>% 
  left_join( hits_to_pathway_ko_table_cleaned, by = c("Species" = "Species", 
                                                      "Strain" = "Strain", 
                                                      "additional_id" = "entity_id") ) %>%
  distinct()
  
write_tsv( transcriptomics_kegg_logFC_FDR_table, path=transcriptomics_kegg_logFC_fdr_values_file ) 

```

## Print the list of non-coding genes and pseudo-genes 
```{r}
de_genes_table_with_ids_master_non_coding_and_pseudo <- de_genes_table_with_ids %>%
  filter( !is.na(pseudo) | 
            ( is.na(protein_id)  & is.na(pseudo) )) %>%
  mutate( Type_of_Experiment = "RNA-Seq") %>%
  mutate( entity_id =  case_when ( str_detect(ID, "_") ~ str_replace( ID, "^gene-", "" ),
                                   str_detect(gene_ids, "_") ~ str_replace( gene_ids, "^gene-", "" ),
                                   str_detect(transcript_id, "_") ~ str_replace( transcript_id, "^gene-", "" ),
                                   TRUE ~ NA_character_ ) ) %>%
  dplyr::distinct( Species, Strain, Type_of_Experiment, entity_id, Comparisons, logFC, FDR )       


write_tsv( de_genes_table_with_ids_master_non_coding_and_pseudo, path=transcriptomics_non_coding_and_psedo_genes_table_file ) 

```

