#!/usr/bin/bash

list_of_files=`grep 'gff.gz' /home/ignatius/ownCloud/Sepsis/Transcriptomics/bpa_sepsis_summary/README.html | sed -e 's/.*\(ftp.*.gz\).*/\1/g'`

cd '/home/ignatius/ownCloud/Sepsis/Annotations_and_Mapping/GFF'

wget $list_of_files

gunzip *.gz

# Go to https://www.ncbi.nlm.nih.gov/assembly/GCF_900622505.1 and see the URL for 'Download the full sequence report' to get the directories structure
wget ftp.ncbi.nlm.nih.gov/genomes/all/GCF/900/622/505/GCF_900622505.1_4559/GCF_900622505.1_4559_genomic.gff.gz   

