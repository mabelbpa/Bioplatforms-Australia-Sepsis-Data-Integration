#/usr/bin/perl -e

use warnings;
use IO::Uncompress::Unzip ();

my $hmdb_dir = "/home/ignatius/ownCloud/Sepsis/Metabolomics/HMDB";

my $hmdb_metabolites_file = "http://www.hmdb.ca/system/downloads/current/hmdb_metabolites.zip";

if( ! -e "$hmdb_dir/hmdb_metabolites.zip" ) {
  system( "wget $hmdb_metabolites_file -O $hmdb_dir/hmdb_metabolites.zip");
}

if (  ! -e "$hmdb_dir/hmdb_metabolites.xml") {
  system( "unzip $hmdb_dir/hmdb_metabolites.zip -d $hmdb_dir");
}

my $input_file  = "$hmdb_dir/hmdb_metabolites.xml";
my $output_file  = "$hmdb_dir/long_format_accession_and_external_accession.tsv";

open( INPUT_FILE , "< $input_file") or die "Cannot open input file $input_file";

open( OUTPUT_FILE , "> $output_file") or die "Cannot open input file $input_file";

my $accession = "";

print OUTPUT_FILE "hmdb_id\texternal_id_type\texternal_id\n";

while( <INPUT_FILE>) {
  
  chomp; 
  
  if( $_ =~/^\s{2}<(accession|pubchem_compound_id|chebi_id|kegg_id|metlin_id)/  ) {

        if ( m/accession/) {
          
          $_ =~ s/<accession>//;
          $_ =~ s/<\/accession>//;
          
          $accession = $_;
          $accession =~ s/\s+//g;
          
        } else {
          
            if( $_ !~ /\/>/) {
                $_ =~ m/<(.*)>(.*)<\/.*>/;
            
                 if( $2 ne "" ) {
                           print OUTPUT_FILE $accession, "\t", $1, "\t", $2, "\n";
        
                 }
             }
        }
  }
}


close(INPUT_FILE);
close(OUTPUT_FILE);

if( -e "$hmdb_dir/hmdb_metabolites.zip" ) {
  system( "rm $hmdb_dir/hmdb_metabolites.zip");
}

