---
title: "R Notebook"
output: html_notebook
---

## Libraries management 
```{r}
if( !require(pacman)) {
  install.packages("pacman")
  library(pacman)
}

p_load(tidyverse)
p_load(iheatmapr)
p_load(tidyselect)
p_load(readxl)
p_load(magrittr)
p_load(multidplyr)
p_load(limma)
p_load(GO.db)


# devtools::install_github("tidyverse/multidplyr")

base_dir <- here::here() # "/home/ignatius/PostDoc/2019/Sepsis"
source( file.path( base_dir, "Source/Common/helper_functions.R") )
source( file.path( base_dir, "Source/Common/common_parameters.R") )

```


nohup time Rscript --vanilla -e "rmarkdown::render('camera_test.Rmd', output_file='camera_test.html') " > camera_test.log 2>&1 &


## Global parameter
```{r}
cluster <- new_cluster(8)
cluster_library( cluster, packages = c("tidyverse", "here", "tidyr", "magrittr"))
my_fdr_threshold <- 0.05
version_date <- master_table_version_date

results_version <- "20191106"

## Parameters for the maximum and minimum size of gene sets
my_minGSSize <- 10
my_maxGSSize <- 500
```

## Directory Management 
```{r}
camera_test_results_dir <- file.path( owncloud_dir,  
               "Multi-omics/Results/GO_Enrichments/Camera_Test",  results_version) 

create_dir_if_not_exists(camera_test_results_dir)

transcriptomics_data_dir <- file.path( owncloud_dir, "Transcriptomics")
transcriptomics_master_table_dir <- file.path( owncloud_dir, "Master_Tables", version_date, "Transcriptomics" ) 
transcriptomics_abundances_dir <- file.path( transcriptomics_master_table_dir, "Abundances" ) 


abundances_master_table_dir <- file.path( owncloud_dir, "Master_Tables", version_date, 
                                          "Multi-omics/Abundances" ) 

```

## Files Management
```{r}

kegg_map_title_file <- file.path( owncloud_dir, 
                                     "Annotations_and_Mapping/KEGG_data_files/pathway/map_title.tab" ) 

kegg_map_title <- vroom::vroom( kegg_map_title_file, 
                                col_names = c("kegg_pathway_id", 
                                              "kegg_pathway_name") )

# To be updated 
# abundances_table_file <- file.path(transcriptomics_abundances_dir, 
#                                    "transcriptomics_log_cpm_abundances_values.tsv" )

abundances_table_file <- file.path( abundances_master_table_dir, 
                                    "multi_omics_master_heatmap_table.tsv")

# refseq_id_to_kegg_pathway_id_file <- file.path( owncloud_dir, 
#                          "Annotations_and_Mapping/KEGG_Annotations_Stable_Versions", 
#                          "Version_2/E_value_0.001/Map_to_Uniprot/refseq_id_to_kegg_pathway_id_table.tsv" )

refseq_id_to_go_annotation_file <- file.path( owncloud_dir, 
                                     "Annotations_and_Mapping/Blast2Go-Results/20191101/blast2go_go_propagation.txt" ) 

## List of GO terms missing from GO.db package, which was manually obtained online
updated_go_terms_file <- file.path( owncloud_dir, 
                               "Annotations_and_Mapping/Blast2Go-Results/20191101/go_terms_updated_terms_edited.tsv" )

list_of_samples_file <- file.path( data_dir, "Samples_List/transcriptomics_samples_list.tsv" )


metabolite_id_to_kegg_pathway_id_file <- file.path( owncloud_dir, 
            "Master_Tables/20190807/Metabolomics/Fold_Changes/metabolomics_kegg_logFC_FDR_table.tsv" ) 


camera_test_full_output_file <- file.path( camera_test_results_dir, 
                                                  "GO_camera_test_all_results.tsv")

camera_test_full_output_rds_file <- file.path( camera_test_results_dir, 
                                                      "GO_camera_test_all_results.RDS")

```

## Read the list of samples
```{r}

list_of_samples <- vroom::vroom( list_of_samples_file ) 

```



## Read Genome statistics
I thought I will be a bit more thorough in listing the columns descriptions (more than usual) for this section because it could be hard to understand without these information or opening the Excel file containing the information. 
```{r}

## The Excel file containing a number of tables with information about the Genomes
genome_statistics_file <- file.path( data_dir, "Genomes_List/num_seq_per_strain_and_accession.xlsx") 

# table: strain_to_accessions
# Description: Get information about the Species and Strain name as compared to the Chromosome/Plasmid RefSeq ID
# Column 1: Species, text, e.g. Streptococcus pyogenes	
# Column 2: Strain, text, e.g. HKU419
# Column 3: refseq_ids, text, e.g. NZ_LR595848.1; NZ_LR595849.1	
#  (The chormosome RefSeq ID. Note that there could be multiple RefSeq ID per line separate by semi-colons and spaces.
#   Some cleaning is done below to clean this up).
# Column 4: Species_strain_file_name, e.g. Streptococcus.pyogenes.HKU419.faa	
# Column 5: List_of_files_to_concat, e.g. NZ_LR595848.1.faa NZ_LR595849.1.faa 
#           (The list of files to concatenate together, including the files containing the chromosomes and plasmids)
strain_to_accessions   <- read_xlsx(path=genome_statistics_file, sheet="strain_to_accessions")

# Table: chromosome_id_to_protein_id
# Description: Get all the Protein RefSeq ID associated with each Chromosome / Plasmid RefSeq ID
# Column 1: chromosome_refseq_id
# Column 2: protein_refseq_id
# Column 3: protein_description
chromosome_id_to_protein_id <- read_xlsx(path=genome_statistics_file, sheet="chromosome_id_to_protein_id")

# Table: strain_to_accessions_cleaned
# Description: Convert Species and Strain name to chromosome/plasmid RefSeq ID. 
#   Clean the refseq_id column using the 'separate_rows' fucntion such that there 
#   is only one refseq_id per row. 
# Column 1: Species, text, e.g. Streptococcus pyogenes	
# Column 2: Strain, text, e.g. HKU419
# Column 1: chromosome_refseq_id, character, e.g. NZ_LR129840.1
strain_to_accessions_cleaned <- strain_to_accessions %>%
                                dplyr::select( Species, Strain, refseq_ids) %>%
                                separate_rows( refseq_ids, sep = "; ") %>%
                                dplyr::rename( chromosome_refseq_id = "refseq_ids")

strain_to_protein_id <- strain_to_accessions_cleaned %>%
  left_join(chromosome_id_to_protein_id, by =c( "chromosome_refseq_id" = "chromosome_refseq_id")) %>%
  dplyr::rename( entity_id = "protein_refseq_id")

list_of_strains <- strain_to_protein_id %>%
                   distinct( Species, Strain)


```

## Group map
# https://dplyr.tidyverse.org/reference/group_map.html


## Add the locus tags into the tables
```{r}
genes_list <-  vroom::vroom( file.path( owncloud_dir, "Master_Tables/20190807/Transcriptomics/Fold_Changes/genes_list.tsv" )  )


refseq_id_to_locus_tag <- genes_list %>%
                          dplyr::distinct( Species, Strain, protein_id, locus_tag ) 

```

## Import GO annotations
```{r}

refseq_id_to_go_annotation_table <- vroom::vroom( refseq_id_to_go_annotation_file )

entity_id_to_go_id_table_protein <- refseq_id_to_go_annotation_table  %>%
                         left_join( strain_to_protein_id, by = c( "Seq Name" = "entity_id" )   ) %>%
                         dplyr::select( -protein_description, -chromosome_refseq_id, -`Seq Description` ) %>%
                         dplyr::rename( entity_id = "Seq Name") %>%
                         dplyr::rename( go_id = "GO") %>%
                         dplyr::mutate( Level = str_replace(Level, "\r", "") %>% as.integer) %>%
                         dplyr::filter( Level > 1) %>%
                         dplyr::distinct( Species, Strain, go_id, entity_id) %>%
                         mutate( entity_type= "Protein")


entity_id_to_go_id_table_transcript <- entity_id_to_go_id_table_protein %>%
  left_join( refseq_id_to_locus_tag, by = c( "Species" = "Species", 
                                             "Strain" = "Strain",
                                             "entity_id"  = "protein_id" ) )  %>%
  dplyr::rename( additional_id = "entity_id" ) %>%
  dplyr::rename(  entity_id = "locus_tag" ) %>%
  dplyr::select( - additional_id ) %>%
  mutate( entity_type = "Transcript") %>%
  dplyr::select( "Species", "Strain",
                 "go_id",
                 "entity_id",
                 "entity_type")


entity_id_to_go_id_table_unfiltered <- entity_id_to_go_id_table_protein %>%
  bind_rows(entity_id_to_go_id_table_transcript)


## Get a list of KEGG gene and metabolite sets to include since the
## size of the gene set is within specified limits
protein_go_sets_to_include <- entity_id_to_go_id_table_unfiltered %>%
  group_by(Species, Strain, 
           go_id, entity_type) %>%
  summarise( counts= n()) %>%
    ungroup %>%
    arrange( Species, Strain, desc(counts)) %>%
  dplyr::filter( counts >= my_minGSSize &
                 counts <= my_maxGSSize)

entity_id_to_go_id_table <- entity_id_to_go_id_table_unfiltered %>%
    dplyr::inner_join( protein_go_sets_to_include, 
                       by = c("Species", "Strain", "go_id", "entity_type")) %>%
    dplyr::select(-counts)

entity_id_to_go_id_table %>% 
  distinct( Species, Strain )

refseq_id_to_go_annotation_table  %>%
  left_join( strain_to_protein_id, by = c( "Seq Name" = "entity_id" )   )  %>% 
  distinct( Species, Strain )

refseq_id_to_go_annotation_table %>%
  distinct( `Seq Name` ) %>%
  count()
```

## Extract all GO IDs, GO Terms and GO type
```{r}
goterms <- Term(GOTERM)

gotypes <- Ontology(GOTERM)

go_term_table <- as.data.frame( goterms  )  %>%
  tibble::rownames_to_column("go_id")  %>%
  dplyr::rename( go_term =  "goterms" )

go_type_table <- as.data.frame( gotypes  )  %>%
  tibble::rownames_to_column("go_id")  %>%
  dplyr::rename( go_type =  "gotypes" )

go_term_description <- entity_id_to_go_id_table %>% 
                       distinct( go_id) %>%
                       left_join( go_term_table , by ="go_id") %>%
                       left_join( go_type_table, by="go_id") %>%
                       mutate( go_type = as.character(go_type),
                               go_term = as.character(go_term))

# go_term_description %>%
#   filter(is.na(go_term) ) %>%
#   dplyr::select(go_id) %>%
#   dplyr::arrange(go_id ) %>%
#   write_tsv( "~/Desktop/go_terms.txt")

updated_go_terms <- vroom::vroom(updated_go_terms_file, col_types = "ccc" )

go_term_description_updated <- go_term_description %>%
  left_join( updated_go_terms, by="go_id") %>%
  mutate( go_term = purrr::map2_chr(  go_term.x, go_term.y, coalesce  ) ) %>%
  mutate( go_type = purrr::map2_chr(  go_type.x, go_type.y, coalesce  ) ) %>%
  dplyr::select( go_id, go_term, go_type)


entity_id_to_go_id_table_updated <- entity_id_to_go_id_table %>%
  left_join( go_term_description_updated, by="go_id") %>%
  dplyr::select( -go_term) 


# entity_id_to_go_id_table_updated_list$data[[1]]


```



## Read abundances master table (draft stage)
```{r}

abundances_table <- vroom::vroom( abundances_table_file )  %>% 
                    dplyr::mutate(  Log_Counts = case_when( is.na( Log_Counts)  ~ 0,
                                                            TRUE ~ Log_Counts) ) %>%
                    dplyr::filter( Type_of_Experiment == "RNA-Seq") %>%
                    dplyr::filter ( Species != "Streptococcus pneumoniae") %>%
                    dplyr::select( -Imputed, -additional_id )


distinct(abundances_table, Species, Strain)


abundances_table %>% filter (is.na( Log_Counts))

```

## Get the experimental design table 
```{r}

design_table <- abundances_table %>%
  distinct( Species, Strain, replicate_name, Treatment_Type, Type_of_Experiment)


design_table %>% distinct( Treatment_Type)

```


## Create a list of gene sets, each element in the list is a list of genes contained in a gene set
```{r}

convert_go_table_to_list_v2 <- function(input_table){ 

  temp_table <- input_table %>%
              group_by ( go_id) %>%
              dplyr::summarise( gene_set  = paste(entity_id, collapse=",")   ) %>%
              dplyr::mutate( gene_set = str_split( gene_set, ",") ) %>%
              ungroup(go_id)
  
  my_list <- temp_table$gene_set 
  names(my_list) <-  temp_table$go_id
  
  return(my_list)
}

list_of_gene_sets_camera <-  entity_id_to_go_id_table_updated %>%
        group_by ( Species, Strain, entity_type, go_type) %>%
        nest(data = c(go_id, entity_id)) %>%
        dplyr::mutate( index = purrr::map ( data, 
                                            convert_go_table_to_list_v2   )) %>%
                             dplyr::select( -data ) %>%
                             ungroup( Species, Strain, entity_type, go_type  )

```

## Format the design matrix list 
```{r}

get_design_matrix <- function(input_table) {
  
  input_table <- input_table %>%
          arrange( replicate_name)
  
  Group <- factor(input_table$Treatment_Type , levels = sort(unique(input_table$Treatment_Type)))
  design_matrix<- model.matrix( ~ 0+Group )
  colnames(design_matrix) <-  c(levels(Group) )
  rownames(design_matrix ) <- input_table$replicate_name
  
  return( design_matrix)
  
}

design_table_list <- design_table %>%
    arrange( Species, Strain, replicate_name ) %>%
    dplyr::mutate( Treatment_Type = str_replace_all( Treatment_Type, " ", "_")%>%
                                    str_replace( "\\+", "plus")  ) %>%
    group_by( Species, Strain, Type_of_Experiment ) %>%
    nest( data = c (  replicate_name, Treatment_Type) )  %>%
    dplyr::mutate( design_matrix = purrr::map(data , get_design_matrix )    ) %>%
    dplyr::select(-data ) %>%
    ungroup( Species, Strain, Type_of_Experiment)

design_table %>% distinct(Species, Strain, Treatment_Type)
    
# design_table_list$design_matrix[[64]]   
 
# [[1]]
# [1] "RPMI" "Sera"
# 
# [[2]]
# [1] "RPMI_plus galactose" "RPMI_plus glucose"   "Sera"               
# 
# [[3]]
# [1] "Galactose" "Glucose"   "Sera"     

```



## Format the list of abundances values for each strain and each type of comparisons
```{r}
my_pivot_norm_expr_matrix <- function( input_table) {
  
  output_matrix <- input_table %>%
    arrange( replicate_name ) %>%
    pivot_wider( names_from = replicate_name, 
                 values_from = Log_Counts) %>%
    column_to_rownames( "entity_id") %>%
    as.matrix()
  
  output_matrix[is.na(output_matrix)] <- 0
  
  return( output_matrix)
  
}

list_of_abundances_table <- abundances_table %>%
  dplyr::select(-Treatment_Type, -Units ) %>%
  distinct() %>%
  group_by( Species, Strain, Type_of_Experiment ) %>%
  nest( data = c( entity_id, replicate_name, Log_Counts)) %>%
  mutate( norm_expr_matrix = purrr::map( data, my_pivot_norm_expr_matrix )   ) %>%
  dplyr::select(-data) %>%
  ungroup( Species, Strain, Type_of_Experiment )

# list_of_abundances_table$norm_expr_matrix[[1]]

```


## Setup the list of contrasts
```{r}

lists_of_contrasts_vectors_temp <- list ( Sera_vs_RPMI = c( -1, 1),
                                          Sera_vs_RPMI_Glucose = c( 0, -1, 1),
                                          Sera_vs_RPMI_Galactose = c( -1, 0, 1),
                                          RPMI_Galactose_vs_RPMI_Glucose = c( 1, -1, 0) )

lists_of_contrasts_vectors <- data.frame( contrast=names(lists_of_contrasts_vectors_temp)) %>%
                                mutate( contrast_vector =   lists_of_contrasts_vectors_temp  )

lists_of_contrasts_strains <- data.frame ( Species = c( "Escherichia coli",
    "Klebsiella variicola",
    "Klebsiella pneumoniae",
    "Staphylococcus aureus",
    "Streptococcus pneumoniae",
    "Streptococcus pneumoniae",
    "Streptococcus pneumoniae",
    "Streptococcus pyogenes" ) ,
      contrast = c(  "Sera_vs_RPMI",
                     "Sera_vs_RPMI",
                     "Sera_vs_RPMI",
                     "Sera_vs_RPMI",
                     "Sera_vs_RPMI_Glucose",
                     "Sera_vs_RPMI_Galactose",
                     "RPMI_Galactose_vs_RPMI_Glucose",
                     "Sera_vs_RPMI") ) 

lists_of_contrasts <- lists_of_contrasts_strains %>%
              left_join( lists_of_contrasts_vectors, by = "contrast")

## Show list of unique design table column names
purrr::map( design_table_list$design_matrix, colnames) %>% unique()

```

```{r}

camera_parameters_table <- list_of_abundances_table %>%
  mutate( entity_type = case_when(  str_detect( Type_of_Experiment, "Metabolomics") ~ 'Metabolite',
                                    str_detect( Type_of_Experiment, "RNA-Seq") ~ 'Transcript',
                                    TRUE ~ "Protein") ) %>%
  left_join( list_of_gene_sets_camera, 
             by = c("Species", "Strain", "entity_type"))  %>%
  left_join( design_table_list, 
             by = c("Species", "Strain", "Type_of_Experiment") ) %>%
  left_join( lists_of_contrasts, by = "Species")

head( camera_parameters_table)

```



```{r}

camera_test_output <- camera_parameters_table %>%
                       mutate( camera_tbl =  purrr::pmap( list( y= norm_expr_matrix,
                                                                index= index, 
                                                                design = design_matrix,
                                                                contrast = contrast_vector)
                                                                , camera     ) ) 

# Error in qr.qty(QR, t(y)) : NA/NaN/Inf in foreign function call (arg 5)
# Similar problem in: 
# https://support.bioconductor.org/p/65442/
# https://github.com/davismcc/scater/issues/69

camera(camera_parameters_table$norm_expr_matrix[[1]],
       camera_parameters_table$index[[1]],
       camera_parameters_table$design_matrix[[1]],
       camera_parameters_table$contrast_vector[[1]])

# qr.qty(qr( camera_parameters_table$norm_expr_matrix[[1]]), t(camera_parameters_table$norm_expr_matrix[[1]]))


which(is.na(  camera_parameters_table$norm_expr_matrix[[1]]))

# camera_test_output$camera_tbl[[1]]


# saveRDS(camera_test_output, file=camera_test_full_output_rds_file   )


# camera_parameters_table$design_matrix[[11]]
# camera_parameters_table$contrast_vector[[11]]
# camera_parameters_table$norm_expr_matrix[[38]]

# which( is.na( camera_parameters_table$norm_expr_matrix[[38]]) )
# camera_parameters_table$norm_expr_matrix[[38]] %>% as.tibble()
# camera_parameters_table$norm_expr_matrix[[38]][1309]

```



```{r}

camera_test_output_unnested <- camera_test_output %>%
  mutate( camera_tbl = purrr::map(camera_tbl, ~rownames_to_column(., "go_id"))) %>%
  dplyr::select(- norm_expr_matrix, - index, -design_matrix, -contrast_vector ) %>%
  unnest(camera_tbl) %>%
  left_join( go_term_description_updated, by = c("go_id", "go_type" ) )


write_tsv( camera_test_output_unnested, 
           path=camera_test_full_output_file)


```



```{r}
set_not_na <- camera_test_output_unnested %>%
  dplyr::filter ( !is.na(FDR)) %>%
  distinct( Species, Strain, Type_of_Experiment, contrast) 


set_is_na <- camera_test_output_unnested %>%
  filter ( is.na(FDR)) %>%
  distinct( Species, Strain, Type_of_Experiment, contrast) 


dplyr::setdiff( set_not_na, set_is_na)
dplyr::setdiff( set_is_na, set_not_na)
dplyr::intersect( set_is_na, set_not_na)


```







