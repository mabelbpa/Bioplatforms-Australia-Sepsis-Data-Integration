#!/bin/bash

BASE_DIR=/home/ignatius/PostDoc/2019/Sepsis
SOURCE_DIR=$BASE_DIR/Source


### Camera Test
KEGG_CAMERA=$SOURCE_DIR/KEGG_Enrichments/camera_test 

### clusterProfiler GSEA function 
KEGG_GSEA=$SOURCE_DIR/KEGG_Enrichments/clusterProfiler_gsea_test 

### Metabolomics MetaboAnalystR Global Test
KEGG_GLOBAL_TEST=$SOURCE_DIR/KEGG_Enrichments/Metabolomics/run_pathways_enrichment_all_species 
 
## GO enrichments 

### Camera Test
GO_CAMERA=$SOURCE_DIR/GO_Enrichments/camera_test 

### clusterProfiler GSEA function 
GO_GSEA=$SOURCE_DIR/GO_Enrichments/clusterProfiler_gsea_test 



Rscript --vanilla -e "rmarkdown::render('$KEGG_CAMERA.Rmd', output_file='$KEGG_CAMERA.html') " > $KEGG_CAMERA.log 2>&1 
Rscript --vanilla -e "rmarkdown::render('$KEGG_GSEA.Rmd', output_file='$KEGG_GSEA.html') " > $KEGG_GSEA.log 2>&1 
Rscript --vanilla -e "rmarkdown::render('$KEGG_GLOBAL_TEST.Rmd', output_file='$KEGG_GLOBAL_TEST.html') " > $KEGG_GLOBAL_TEST.log 2>&1 
Rscript --vanilla -e "rmarkdown::render('$GO_CAMERA.Rmd', output_file='$GO_CAMERA.html') " > $GO_CAMERA.log 2>&1 
Rscript --vanilla -e "rmarkdown::render('$GO_GSEA.Rmd', output_file='$GO_GSEA.html') " > $GO_GSEA.log 2>&1 



tail $KEGG_CAMERA.log 
tail $KEGG_GSEA.log
tail $KEGG_GLOBAL_TEST.log
tail $GO_CAMERA.log
tail $GO_GSEA.log











