* I have also made some changes to the fold-change table.
* I have added an additional_id column to the fold-change table.
* For the transcript will have the 'locus tag', the proteins will have the 'locus tag, a list separated by semi-colon if a protein is associated with several locus'.  For the metabolites, the 'additional_id' column will have the HMDB ID or the entity name.
* I have also made some changes to the abundances table.
* The entity_id for proteins = Refseq ID (e.g. WP_*)
* The entity_id for metabolites = metabolite name
* Entity_id for transcripts = locus tag
* I also added the 'additional_id' column for the abundances table.
* The additional_id for proteins = 'locus tag, a list separated by semi-colon if a protein is associated with several locus'.
* The additional_id for transcripts = Refseq ID (e.g. WP_*)
* The additional_id for metabolites = HMDB ID or the entity name.
